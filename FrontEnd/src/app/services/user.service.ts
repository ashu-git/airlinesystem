import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from '../models/login';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private httpclient: HttpClient) { }

  RegisterUser(user: User): Observable<boolean> {
    return this.httpclient.post<boolean>('https://localhost:44309/api/User/AddUser', user)
  }

  LoginUser(login: Login):Observable<string> {
    return this.httpclient.post<string>('https://localhost:44309/api/User/Login',login)
  }
}
