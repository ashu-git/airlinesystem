import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plane } from '../models/plane';

@Injectable({
  providedIn: 'root'
})
export class PlaneService {
  constructor(private httpclient: HttpClient) { }

  getAllPlanes() {
    return this.httpclient.get<Plane[]>('https://localhost:44358/api/Plane/GetAllPlain')
  }
}
