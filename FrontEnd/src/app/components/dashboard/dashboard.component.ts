import { Component, OnInit } from '@angular/core';
import { Plane } from 'src/app/models/plane';
import { PlaneService } from 'src/app/services/plane.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  planes?:Plane[]
  constructor(private planeservice: PlaneService) { }
  
  ngOnInit(): void {
    
    this.planeservice.getAllPlanes().subscribe(res=>{
      console.log(res);
      this.planes =res
    })
  }
}
