﻿using Plane_API_for_Airline_Reservation_System.Context;
using Plane_API_for_Airline_Reservation_System.Models;
using System.Xml.Linq;

namespace Plane_API_for_Airline_Reservation_System.Repository
{
    public class PlaneRepository : IPlaneRepository
    {
        readonly AirlineReservationSystemContextDb _airlineReservationSystemContextDb;
        public PlaneRepository(AirlineReservationSystemContextDb airlineReservationSystemContextDb)
        {
            _airlineReservationSystemContextDb = airlineReservationSystemContextDb;
        }

        //Add Plain
        public void AddPlain(Plane plain)
        {
            _airlineReservationSystemContextDb.Add(plain);
            _airlineReservationSystemContextDb.SaveChanges();
        }

        //Delete Plain
        public void Delete(int id)
        {
            _airlineReservationSystemContextDb.Remove(GetPlainById(id));
            _airlineReservationSystemContextDb.SaveChanges();
        }

        //Details Plain
        public Plane Details(int id)
        {
            Plane searchPlain = GetPlainById(id);
            return searchPlain;
        }

        //Get All Plain
        public List<Plane> GetAllPlain()
        {
            return _airlineReservationSystemContextDb.planes.ToList();
        }

        //Get Plain By Id
        public Plane GetPlainById(int id)
        {
            return _airlineReservationSystemContextDb.planes.Where(c => c.PlaneId == id).FirstOrDefault();
        }

        //Get Plain By Name
        public Plane GetPlainByName(string name)
        {
            return _airlineReservationSystemContextDb.planes.Where(c => c.PlaneName == name).FirstOrDefault();
        }
    }
}
