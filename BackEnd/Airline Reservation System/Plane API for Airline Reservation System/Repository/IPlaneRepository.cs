﻿using Plane_API_for_Airline_Reservation_System.Models;

namespace Plane_API_for_Airline_Reservation_System.Repository
{
    public interface IPlaneRepository
    {
        void AddPlain(Plane plain);
        void Delete(int id);
        Plane Details(int id);
        List<Plane> GetAllPlain();
        Plane GetPlainById(int id);
        Plane GetPlainByName(string name);
    }
}
