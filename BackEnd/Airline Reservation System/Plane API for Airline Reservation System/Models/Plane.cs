﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Plane_API_for_Airline_Reservation_System.Models
{
    public class Plane
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlaneId { get; set; }

        [Required]
        public string PlaneName { get; set; }

        [Required]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        public string PlaneNumber { get; set; }

        [Required]
        public string Price { get; set; }
    }
}
