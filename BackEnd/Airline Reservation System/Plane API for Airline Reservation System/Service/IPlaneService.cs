﻿using Plane_API_for_Airline_Reservation_System.Models;

namespace Plane_API_for_Airline_Reservation_System.Service
{
    public interface IPlaneService
    {
        bool AddPlain(Plane plain);
        bool Delete(int id);
        Plane Details(int id);
        List<Plane> GetAllPlain();
    }
}
