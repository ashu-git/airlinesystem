﻿using Plane_API_for_Airline_Reservation_System.Models;
using Plane_API_for_Airline_Reservation_System.Repository;

namespace Plane_API_for_Airline_Reservation_System.Service
{
    public class PlaneService : IPlaneService
    {
        readonly IPlaneRepository _planeRepository;
        public PlaneService(IPlaneRepository planeRepository)
        {
            _planeRepository = planeRepository;
        }

        public List<Plane> GetAllPlain()
        {
            return _planeRepository.GetAllPlain();
        }

        //Add Course
        public bool AddPlain(Plane plain)
        {
            Plane PlainAddExistStatus = _planeRepository.GetPlainByName(plain.PlaneName);
            if (PlainAddExistStatus == null)
            {
                _planeRepository.AddPlain(plain);
            }
            return true;
        }

        public bool Delete(int id)
        {
            Plane PlainDeleteExistsStatus = _planeRepository.GetPlainById(id);
            if (PlainDeleteExistsStatus != null)
            {
                _planeRepository.Delete(id);
            }
            return true;
        }

        public Plane Details(int id)
        {
            return _planeRepository.Details(id);
        }
    }
}
