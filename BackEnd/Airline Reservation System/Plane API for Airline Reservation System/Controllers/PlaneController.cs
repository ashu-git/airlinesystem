﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Plane_API_for_Airline_Reservation_System.Models;
using Plane_API_for_Airline_Reservation_System.Service;

namespace Plane_API_for_Airline_Reservation_System.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class PlaneController : ControllerBase
    {
        readonly IPlaneService _planeService;

        public PlaneController(IPlaneService planeService)
        {
            _planeService = planeService;
        }

        //Get All Plain
        [Route("GetAllPlain")]
        [HttpGet]
        public ActionResult GetAllPlain()
        {
            List<Plane> AllPlane = _planeService.GetAllPlain();
            return Ok(AllPlane);
        }

        //Add Plain
        [Route("AddPlain")]
        [HttpPost]
        public ActionResult AddPlain(Plane plain)
        {
            bool addPlainStatus = _planeService.AddPlain(plain);
            return Created("addPlainStatus", addPlainStatus);
        }

        //Delete User
        [Route("DeletePlain")]
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            bool deletePlainStatus = _planeService.Delete(id);
            return Ok(deletePlainStatus);
        }

        //Details
        [Route("DetailsPlain")]
        [HttpGet]
        public ActionResult Details(int id)
        {
            Plane detailsPlainStatus = _planeService.Details(id);
            return Ok(detailsPlainStatus);
        }
    }
}
